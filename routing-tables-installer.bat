@ECHO OFF
SETLOCAL

REM LEGACY SCRIPT - DO NOT USE
REM USE THE NEW POWERSHELL VERSION INSTEAD

:Admin
NET session >NUL 2>&1
IF ERRORLEVEL 1 (
	ECHO Please run this script as admin.
	ECHO The route command requires admin priviliges!
	GOTO End
)

ECHO This script adds or removes xivalexander routes for FF14.
CHOICE /N /C:12 /M "Add rules [1] or delete rules [2]: "%1
IF ERRORLEVEL ==2 GOTO UNINSTALL
IF ERRORLEVEL ==1 GOTO INSTALL

:INSTALL
SET /P server-ip=Set server IP (ex: 192.168.1.171): 
netsh int ipv4 show interfaces
SET /P interface=Pick a network interface ID: 
SET /P netmask=Choose a netmask address (Optional): 
ECHO.

SET interface=IF %interface%
IF "%netmask%" == "" SET netmask=255.255.255.0

route add -p 204.2.229.0 mask %netmask% %server-ip% %interface%
route add -p 124.150.157.0 mask %netmask% %server-ip% %interface%
route add -p 183.111.189.0 mask %netmask% %server-ip% %interface%
route add -p 195.82.50.0 mask %netmask% %server-ip% %interface%
route add -p 153.254.80.0 mask %netmask% %server-ip% %interface%
route add -p 202.67.52.0 mask %netmask% %server-ip% %interface%
route add -p 80.239.145.0 mask %netmask% %server-ip% %interface%

ECHO Install completed.
GOTO End

:UNINSTALL
route delete -p 204.2.229.0
route delete -p 124.150.157.0
route delete -p 183.111.189.0
route delete -p 195.82.50.0
route delete -p 153.254.80.0
route delete -p 202.67.52.0
route delete -p 80.239.145.0
ECHO Uninstall completed.
GOTO End

:End
PAUSE
