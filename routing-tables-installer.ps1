#Requires -RunAsAdministrator

function Get-OpCodes {
    $url = "https://api.github.com/repos/Soreepeong/XivAlexander/contents/StaticData/OpcodeDefinition"

    $req = Invoke-WebRequest -URI $url -UseBasicParsing

    if ($req.StatusCode -eq 200) {
        $opCodes = $req.Content | ConvertFrom-Json

        foreach ($region in $opCodes) {
            if ($region.name -match '^game\.JP') {
                $url = $region.url

                $req = Invoke-WebRequest -URI $url -UseBasicParsing

                if ($req.StatusCode -eq 200) {
                    $base64 = ($req.Content | ConvertFrom-Json).content

                    $json = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($base64))

                    New-Item -ItemType File -Path $Env:TEMP -Name "definitions.json" -Value $json -Force -ErrorAction Stop > $null
                } else {
                    echo "Unable to retrieve JP OpCodes: $url"
                }
            }
        }
    } else {
        echo "Unable to retrieve API: $url"
    }
}

function Write-Menu {
    param (
        [string]$Title
    )
    Clear-Host
    Write-Host "=================== $Title ==================="
    Write-Host "1: Press '1' to import game server IPs in the routing table."
    Write-Host "2: Press '2' to delete game server IPs from the routing table."
    Write-Host "Q: Press 'Q' to exit the script."
    Write-Host ""
}

# Check if definitions.json exists
if ([System.IO.File]::Exists("$Env:TEMP\definitions.json")) {
    $lastWrite = (Get-Item $Env:TEMP\definitions.json).LastWriteTime

    $timeSpan = [datetime]::Now.AddDays(-30)

    if ($lastWrite -gt $timeSpan) {
        Get-OpCodes
    }
} else {
    Get-OpCodes
}

# Analyze definitions.json
$defs = Get-Content $Env:TEMP\definitions.json -Raw | ConvertFrom-Json

Write-Menu -Title 'XivMitmLatencyMitigator'

$selection = Read-Host "Please enter an option"

switch($selection) {
    '1' {
        $hostServer = Read-Host "Enter host server IP"
        netsh int ipv4 show interfaces
        $interface = Read-Host "Pick desired network interface ID"
        
        foreach($ip in $defs.Server_IpRange -Split ", ") {
            $ip = $ip -replace "/\d+$", ""
            route add -p $ip mask 255.255.255.0 $hostServer IF $interface
        }
		
		pause
    } '2' {
        Write-Host "Deleting all IPs.."
        foreach($ip in $defs.Server_IpRange -Split ", ") {
            $ip = $ip -replace "/\d+$", ""
            route delete -p $ip
        }
		
		pause
    } 'q' {
        return
    }
}