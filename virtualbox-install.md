For simplicity I'll assume you are going to use [Debian x64 netinst](https://www.debian.org/CD/netinst/)

- Make a new VM in VirtualBox with minimum 512mb of RAM and 3gb of HDD space
- Attach the install `.iso` to `Settings > Storage > Controller IDE > Empty > CDrom icon`
- Move to `Network` tab and set `Attached to` value to `Bridged Adapter`
- Move to `Shared Folders`, add the directory containing `ffxiv.exe` and name the entry `ff14`
- Run the VM and start the install process (non graphical)
- When asked what software to install, only pick `Standard` and `SSH Server`
- Once done login with your root credentials and run the following
  ```bash
  apt update && apt install \
      build-essential dkms linux-headers-$(uname -r) \
      wget iptables g++ gcc-multilib g++-multilib
  ```
- In the VM topbar click on `Devices > Insert guest addition cd image` and run the following
  ```bash
  mount /dev/cdrom /media/cdrom && cd /media/cdrom && sh ./VBoxLinuxAdditions.run
  ```
- Run `nano /etc/ssh/sshd_config`, uncomment `PermitRootLogin`, change its value to `yes` and close the editor
- Run the following to preserve your current IP for future runs:
  ```bash
  IP="$(hostname -i)"
  ROUTER="$(ip route | awk '/default/ {print $3}')"
  INTERFACE="$(ip -br addr | awk '/UP/ {print $1}')"
  sed -i -E "s/(iface $INTERFACE inet) dhcp/\1 static/" /etc/network/interfaces
  echo -e "address $IP\ngateway $ROUTER" >> /etc/network/interfaces
  ```
- Exit the VM by clicking on `File > Close > Send the shutdown signal`
- Enable `auto-mount` and `read-only` in `Settings > Shared Folders > ffxiv directory (double click)`
- Confirm the changes, take note of your VM name and exit the VM manager
- Install [VBoxVMService](https://github.com/onlyfang/VBoxVmService) to handle automatic start/shutdown of your headless VM
- Open `VBoxVmService.ini` with a text editor, delete `[Vm1]` entries and replace the `[Vm0]` ones with the following:
  ```ini
  [Vm0]
  VmName=Name of your Virtual Machine
  ShutdownMethod=acpipowerbutton
  AutoStart=yes
  ```
- Copy `VmServiceTray.exe` and create a shortcut in `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup`
- Restart your system and run this [script](bin/batch/ff14-xivalex-route.bat) to reroute the game traffic to your VM (pick `Idx` of your main network adapter)
- Open the command prompt and connect to your VM with `ssh root@your-VM-ip`
- Prepare a routing mask for the incoming game packets and symlink `ffxiv.exe` with the following:
  ```bash
  INTERFACE="$(ip -br addr | awk '/UP/ {print $1}')"
  ADDRESS="$(ip -4 -br addr | awk '/UP/ {print $3}')"
  iptables -t nat -A POSTROUTING -s $ADDRESS -o $INTERFACE -j MASQUERADE
  ln -s /media/sf_ff14/ffxiv.exe /root/.
  ```
- Download the XivAlexander [script](https://github.com/Soreepeong/XivMitmLatencyMitigator/blob/main/mitigate.py) in your root directory
  ```bash
  wget https://raw.githubusercontent.com/Soreepeong/XivMitmLatencyMitigator/main/mitigate.py
  ```
- Test the script with `python3 ./mitigate.py` and login with your ff14 character to see if the script reacts to your actions
- Exit the script with `Ctrl+C`, make a startup service with `nano /etc/systemd/system/xivalex.service` and copy-paste the following
  ```ini
  [Unit]
  Description=Run XivAlexander script on boot
  
  [Service]
  Type=simple
  User=root
  WorkingDirectory=/root
  ExecStart=/usr/bin/python3 mitigate.py
  Restart=on-failure
  RestartSec=5
  
  [Install]
  WantedBy=multi-user.target
  ```
- Start the service, check if it runs correctly and if so enable it to automate the process for the future
  ```bash
  systemctl start xivalex
  systemctl status xivalex
  systemctl enable xivalex
  ```
