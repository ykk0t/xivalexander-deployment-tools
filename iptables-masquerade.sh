#!/bin/bash

INTERFACE=$(ip route | awk '/default/ {print $5}')
ADDRESS=$(ip -4 -br addr | awk '/UP/ {print $3}')

echo -e "INTERFACE: $INTERFACE | ADDRESS: $ADDRESS\n"

if [[ $1 == "install" ]]; then
  echo "Adding $ADDRESS to iptables.."
  iptables -v -t nat -A POSTROUTING -s $ADDRESS -o $INTERFACE -j MASQUERADE
elif [[ $1 == "uninstall" ]]; then
  echo "Removing $ADDRESS from iptables.."
  iptables -v -t nat -D POSTROUTING -s $ADDRESS -o $INTERFACE -j MASQUERADE
elif [[ $1 == "list" ]]; then
  iptables -t nat -L POSTROUTING -n -v
  iptables -t nat -L PREROUTING -n -v
elif [[ $1 == "flush" ]]; then
  iptables -t nat -F PREROUTING -v
else
  echo "The script requires an argument: $BASH_SOURCE <install|uninstall|list|flush>"
fi
