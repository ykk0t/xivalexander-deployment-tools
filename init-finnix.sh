#!/bin/bash

# ----------------------------------------------------------- #
#   Install XivAlexander script on a new Finnix environment   #
# ----------------------------------------------------------- #
#       This distro doesn't require a virtual hard disk       #
#   _Minimum_ 1024M of ram allocated to the virtual machine   #
# ----------------------------------------------------------- #
# Tested on 2022-03-22 Finnix build - https://www.finnix.org/ #
# ----------------------------------------------------------- #

# Walkthrough for retards:
# 0. Add a new VM, go to Settings > Storage > Controller : IDE
# 1. Add a 2nd empty optical drive and add Finnix.iso to 1st
# 2. Settings > Network, set mode to "Bridged Adapter"
# 3. Settings > Shared Folders, add path to "ffxiv.exe"
# 4. Toggle on "Read-only" and "Auto-mount" options
# 5. Launch the VM, hit Enter in the menu and wait to login
# 6. Type 0, hit enter and configure your keyboard/timezone
# 7. Top bar > Devices > Insert Guest Additions Cd Image...
# 8. Save this script with curl <raw-script-url> > install.sh
# 9. chmod +x install.sh, and finally run it with .\install.sh

# !! Use snapshotting/save state, when you want to shut down !!
# !! your computer or you will have to repeat steps 6 to 9.. !!

# You can use https://github.com/onlyfang/VBoxVmService to
# handle automatic start/shut down of the virtual machine.
# Just remember to set "ShutdownMethod" value to "savestate"

ROOTPARTITION=/run/live/overlay
INTERFACE=/etc/systemd/network/$(ls /etc/systemd/network | head -n 1)

# Make Live ISO fill the entirety of the allocated memory
mount -o remount,size=1G $ROOTPARTITION

# Mount shared VBox directory
echo "Mounting shared VBox directory.."
mount /dev/sr1 /mnt
cd /mnt
sh ./VBoxLinuxAdditions.run --nox11 &> /dev/null
umount -fl /mnt
cd $HOME

# Symlink game launcher to /root
echo "Fetching game launcher.."
ln -s /media/$(ls /media | head -n 1)/ffxiv.exe $HOME

# Install XivAlexander dependencies
echo "Installing dependencies.."
apt update
apt -y install g++ gcc-multilib g++-multilib

# Fetch launch script URL
echo -e "\nDownloading XivAlexander script.."
echo -e "#!/bin/bash\ncurl https://raw.githubusercontent.com/Soreepeong/XivMitmLatencyMitigator/main/mitigate.py | python3" > run.sh
chmod +x run.sh

# Set a static IP address
sed -i -e "/^DHCP=/s/yes/no/" $INTERFACE
echo -e "Address=\nGateway=\nDNS=" >> $INTERFACE
nano $INTERFACE
service systemd-networkd restart

# Enable SSH for headless support
echo "Set a password for root.."
passwd
echo "Starting SSH service.."
service sshd start

# Add XivAlexander systemd service
echo -e "[Unit]\n\
Description=XivAlexander script\n\
\n\
[Service]\n\
Type=simple\n\
User=root\n\
WorkingDirectory=/root\n\
ExecStart=/bin/bash run.sh\n\
ExecStop=-/bin/bash .cleanup.sh\n\
ExecStop=-/bin/rm .cleanup.sh\n\
\n\
[Install]\n\
WantedBy=multi-user.target" > /etc/systemd/system/xivalexander.service

# Run XivAlexander
service xivalexander start
service xivalexander status